# Nex Demo App

Nex Demo App is a sample application that shows usage of **Nex Band Sdk** and **Nex Light Pattern Editor** libraries. It includes:

  - The code that should add to gradle to include the Nex libraries.
  - The code to initialize band session.
  - Connect to band
  - Send command sequences to band 
  - Create a light pattern in editor and play it on band

Just like demo app, In order to use SDK and Light Pattern Editor in your app, you should do the following:

### Add repository address:
Add these two properties to gradle.properties

```groovy
artifactory_username=YOUR_REPO_USER_NAME
artifactory_password=YOUR_REPO_PASSWORD
```

Add this repository to your project .gradle file:
```groovy
maven {
            url "http://34.199.41.99:8081/artifactory/libs-release-local"
            credentials {
                username = "${artifactory_username}"
                password = "${artifactory_password}"
            }
        }
```
### Add dependencies
```groovy
  implementation "com.mightycast:bandsdk:1.1.4"
  implementation "com.mightycast:lightpatterneditor:1.0.1"
```

**Note**:
Add Java version 8 compatibility to your project by adding **compileOptions** to gradle:
```groovy
android {
  ....
  compileOptions {
    sourceCompatibility JavaVersion.VERSION_1_8
    targetCompatibility JavaVersion.VERSION_1_8
  }
}
```
# SDK Usage 
--------
Now you can initialize and use **Nex Band SDK** and **Light Pattern Editor**  

### Initialize MCBandSession
In order to use band sdk, you must initialize th MCBandSession in your Application.

```java
public class NexDemoApp extends Application {
  @Override public void onCreate() {
    super.onCreate();
    MCBandSession.initialize(this);
  }
}
```

### Connect to band 
To connect to band you should call startConnection.
```java
MCBandSession.startConnection(getContext());
```

### Using Light pattern editor
In order to create a light pattern to play on band you can use Light Pattern Editor. Create an intent of PatternEditorActivity class and do startActivityForResult

```java
    Intent intent = new Intent(getContext(), PatternEditorActivity.class);
    intent.putExtra(PatternEditorActivity.SEQUENCES_BASE64, latestPattern);
    startActivityForResult(intent, 121);
```

Then onActivityResult you can get the designed light pattern as PatternEditorActivity.SEQUENCES_BASE64. 
```java
@Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == RESULT_OK && data.hasExtra(PatternEditorActivity.SEQUENCES_BASE64)) {
      String[] latestPattern = data.getStringArrayExtra(PatternEditorActivity.SEQUENCES_BASE64);
    }
  }
```
You can then convert the latestPattern to CommandSequence:

```java
MCBandCommandSequence sequence = sequenceWithData(Base64.decode(latestPattern[0], Base64.DEFAULT));
```
