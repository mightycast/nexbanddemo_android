package com.mightycast.nexbandsample;

import android.app.Application;
import com.mightycast.bandsdk.model.MCBandSession;

/**
 * Created by O1 on 2017-03-11.
 * You can use NexBandApplication directly in Manifest or you can extend your own and use it instead
 */

public class NexDemoApp extends Application {
  @Override public void onCreate() {
    super.onCreate();
    MCBandSession.initialize(this);
  }
}
