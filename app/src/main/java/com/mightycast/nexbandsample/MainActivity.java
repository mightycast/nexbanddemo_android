package com.mightycast.nexbandsample;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import com.mightycast.bandsdk.model.MCBandSession;
import com.mightycast.bandsdk.util.MCBandUtils;

public class MainActivity extends AppCompatActivity implements BaseFragment.FragmentEventListener {

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    if (savedInstanceState == null) {
      getSupportFragmentManager().beginTransaction()
          .replace(R.id.container, BandConnectFragment.newInstance(),
              BandConnectFragment.class.getName())
          .commit();
    }
  }

  @Override public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
      @NonNull int[] grantResults) {
    if (MCBandUtils.androidApiIsAtleast(23)) {
      if (!shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)
          && !MCBandSession.isLocationPermissionGranted(this)) {
        showNeverAskAgainPermissionAlert();
      }
    }
  }

  private void showNeverAskAgainPermissionAlert() {
    new AlertDialog.Builder(this).setMessage(
        "Location must be enabled in the permission section of App settings.")
        .setNegativeButton(android.R.string.cancel, null)
        .setPositiveButton("Open", new AlertDialog.OnClickListener() {
          @Override public void onClick(DialogInterface dialogInterface, int i) {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", getPackageName(), null);
            intent.setData(uri);
            startActivityForResult(intent, 0x5E11);
          }
        })
        .show();
  }

  //// Interface method to transfer fragment events to activity ////////
  //// When connection was succeed, it goes to next fragment ///////////
  //// When connection was lost, it comes back to connection fragment //
  @Override public void onFragmentEvent(String event) {
    switch (event) {
      case "SHOW_CONNECT_FRAGMENT": {
        getSupportFragmentManager().beginTransaction()
            .replace(R.id.container, BandConnectFragment.newInstance(),
                BandConnectFragment.class.getName())
            .commit();
        break;
      }
      case "SHOW_PLAYS_FRAGMENT": {
        getSupportFragmentManager().beginTransaction()
            .replace(R.id.container, BandPlaysFragment.newInstance(),
                BandPlaysFragment.class.getName())
            .commit();
        break;
      }
    }
  }
}
