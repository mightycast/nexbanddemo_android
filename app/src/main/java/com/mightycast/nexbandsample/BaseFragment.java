package com.mightycast.nexbandsample;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;
import com.mightycast.bandsdk.command.model.MCBandCommandSequence;
import com.mightycast.bandsdk.command.model.MCBandHardwareSequences;
import com.mightycast.bandsdk.command.player.MCBandCommandSequencePlayer2;
import com.mightycast.bandsdk.model.MCBand;
import com.mightycast.bandsdk.util.MCBandLogger;
import java.util.List;

/**
 * Created by mojtabaa on 2017-03-29.
 */

public abstract class BaseFragment extends Fragment {
  private static final String TAG = "BaseFragment";

  public interface FragmentEventListener {
    void onFragmentEvent(String event);
  }

  @Override public void onAttach(Context context) {
    super.onAttach(context);
    IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
    filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
    filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
    filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
    LocalBroadcastManager.getInstance(context).registerReceiver(mBluetoothReceiver, filter);

    IntentFilter connectionFilter = new IntentFilter();
    connectionFilter.addAction(MCBand.ACTION_BASE_DID_CONNECT);
    connectionFilter.addAction(MCBand.ACTION_BASE_DID_DISCONNECT);
    connectionFilter.addAction(MCBand.ACTION_BASE_WILL_CONNECT);
    connectionFilter.addAction(MCBand.ACTION_BASE_WILL_DISCOVER);
    connectionFilter.addAction(MCBand.ACTION_BASE_DID_FAIL_TO_CONNECT);

    MCBandLogger.d(TAG, "Register Receivers");
    LocalBroadcastManager.getInstance(context)
        .registerReceiver(mBaseConnectionReceiver, connectionFilter);
  }

  @Override public void onDestroy() {
    LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBaseConnectionReceiver);
    LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBluetoothReceiver);
    super.onDestroy();
  }

  private final BroadcastReceiver mBaseConnectionReceiver = new BroadcastReceiver() {
    @Override public void onReceive(Context context, Intent intent) {
      String action = intent.getAction();
      MCBandLogger.d(TAG, "------ Band Connection Changed" + action + " --------");
      switch (action) {
        case MCBand.ACTION_BASE_DID_FAIL_TO_CONNECT:
        case MCBand.ACTION_BASE_DID_DISCONNECT:
          // Band Failed to Pair..
          onBandDisconnected();
          break;
        case MCBand.ACTION_BASE_DID_CONNECT:
          onBandConnected();
          playSuccessPattern();

          /// You should enable Motion detector to receive Steps. It's Disabled by default.
          MCBand.getCurrentBand().setMotionEnabled(true);
          break;
      }
    }
  };

  /// It plays a predefined pattern to show connection succeeded ///
  private void playSuccessPattern() {
    Toast.makeText(getContext(), "Band Connected, time to play!", Toast.LENGTH_SHORT).show();
    if (MCBand.isConnected()) {
      List<MCBandCommandSequence> successSequence =
          MCBandHardwareSequences.sequencesFromBase64(MCBandHardwareSequences.CONNECTION_BASE64);
      MCBandCommandSequencePlayer2 player = new MCBandCommandSequencePlayer2();
      player.playSequences(successSequence, MCBand.getCurrentBand(), null);
    }
  }

  private final BroadcastReceiver mBluetoothReceiver = new BroadcastReceiver() {
    @Override public void onReceive(Context context, Intent intent) {
      final String action = intent.getAction();
      MCBandLogger.d(TAG, "------ Bluetooth Adapter Change Action: " + action);
      switch (action) {
        case BluetoothAdapter.ACTION_STATE_CHANGED:
          final int state =
              intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
          switch (state) {
            case BluetoothAdapter.STATE_OFF:
              MCBandLogger.d(TAG, "------ Bluetooth was turned off ------");
              onBluetoothTurnedOff();
              break;
            case BluetoothAdapter.STATE_TURNING_OFF:
              MCBandLogger.d(TAG, "------ Bluetooth is turning off ------");
              onBluetoothTurningOff();
              break;
            case BluetoothAdapter.STATE_ON:
              MCBandLogger.d(TAG, "------ Bluetooth was turned on ------");
              onBluetoothTurnedOn();
              break;
            case BluetoothAdapter.STATE_TURNING_ON:
              MCBandLogger.d(TAG, "------ Bluetooth is turning on ------");
              onBluetoothTurningOn();
              break;
          }
          break;
        case BluetoothDevice.ACTION_ACL_CONNECTED:
          break;
        case BluetoothDevice.ACTION_ACL_DISCONNECTED:

          break;
      }
    }
  };

  public abstract void onBluetoothTurnedOff();

  public abstract void onBluetoothTurningOff();

  public abstract void onBluetoothTurnedOn();

  public abstract void onBluetoothTurningOn();

  public abstract void onBandConnected();

  public abstract void onBandDisconnected();
}
