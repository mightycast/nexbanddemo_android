package com.mightycast.nexbandsample;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import com.mightycast.bandsdk.model.MCBand;
import com.mightycast.bandsdk.model.MCBandSession;
import com.mightycast.bandsdk.util.MCBandLogger;
import com.mightycast.bandsdk.util.MCBandUtils;

import static android.content.ContentValues.TAG;

/**
 * Created by O1 on 2017-03-17.
 */

//// It's inherited from BaseFragment to be aware of band/bluetooth connection events
public class BandConnectFragment extends BaseFragment {
  private static final int CONNECTION_TIMEOUT = 11000;

  private Button btnEnableBluetooth;
  private Button btnRequestLocation;
  private Button btnConnectBand;

  public static BandConnectFragment newInstance() {
    Bundle args = new Bundle();
    BandConnectFragment fragment = new BandConnectFragment();
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {

    View view = inflater.inflate(R.layout.fragment_band_connect, container, false);

    btnEnableBluetooth = (Button) view.findViewById(R.id.btnEnableBlueTooth);
    btnRequestLocation = (Button) view.findViewById(R.id.btnRequestLocationPermission);
    btnConnectBand = (Button) view.findViewById(R.id.btnConnectBand);

    btnConnectBand.setEnabled(false);
    MCBandSession.stopConnection(getContext());
    return view;
  }

  @Override public void onResume() {
    super.onResume();
    verifyBluetoothEnabled();
    verifyLocationPermission();
    verifyReadyToPairBand();
  }

  // Checks whether bluetooth is enabled or not and ask for it if it's not enabled
  private void verifyBluetoothEnabled() {
    if (MCBandUtils.isBLEEnabled(getContext())) {
      btnEnableBluetooth.setEnabled(false);
    } else {

      btnEnableBluetooth.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
          MCBandSession.enableBluetoothIfNeeded(getActivity(), 0x0B1E);
        }
      });
    }
  }

  /// Checks location permission and ask for it if it's not granted ///
  private void verifyLocationPermission() {
    if (MCBandSession.isLocationPermissionGranted(getContext())) {
      btnRequestLocation.setEnabled(false);
    } else {

      btnRequestLocation.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
          MCBandSession.requestLocationPermission(getActivity(), 0x010C);
        }
      });
    }
  }

  private void verifyReadyToPairBand() {
    if (btnEnableBluetooth.isEnabled() || btnRequestLocation.isEnabled()) {
      btnConnectBand.setEnabled(false);
    } else {
      btnConnectBand.setEnabled(true);

      btnConnectBand.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
          connectBand();
        }
      });
    }
  }

  private void connectBand() {
    // If the band is already connected
    if (MCBand.isConnected()) {
      Toast.makeText(getContext(), "Already Paired", Toast.LENGTH_SHORT).show();
      onBandConnected();
      return;
    }

    btnConnectBand.setEnabled(false);

    //We may already have a device bonded from connecting from another app that uses the band
    MCBandSession.startConnection(getContext());

    Toast.makeText(getContext(), "Paring Band, please wait...", Toast.LENGTH_SHORT).show();

    //// Starts scanning to find bluetooth device ///
    //MIGSdkSession.getInstance().setDiscoverBaseListener(new MIGBase.MIGDiscoverBaseListener() {
    //  @Override public void OnScanStarted() {
    //    btnConnectBand.setText("Pairing Band");
    //  }
    //
    //  @Override public void OnScanStopped() {
    //    btnConnectBand.setText("Pair Band");
    //  }
    //
    //  @Override public void OnBaseDiscovered(MIGBase base, int rssi) {
    //    btnConnectBand.setText("Band Discovered");
    //  }
    //
    //  @Override public void OnBaseConnected(MIGBase base, int rssi) {
    //    btnConnectBand.setText("Band Connected");
    //  }
    //
    //  @Override public void OnError(Throwable throwable) {
    //    btnConnectBand.setText("Band connection failed:" + throwable.getMessage());
    //  }
    //});
  }

  // BaseFragment receives the bluetooth events through BroadcastReceiver //
  @Override public void onBluetoothTurnedOff() {
    MCBandLogger.d(TAG, "------ Bluetooth was turned off ------");
    Toast.makeText(getContext(), "Bluetooth was turned off ", Toast.LENGTH_LONG).show();
  }

  // BaseFragment receives the bluetooth events through BroadcastReceiver //
  @Override public void onBluetoothTurningOff() {
    MCBandLogger.d(TAG, "------ Bluetooth is turning off ------");
    btnEnableBluetooth.setEnabled(true);
    verifyReadyToPairBand();
  }

  // BaseFragment receives the bluetooth events through BroadcastReceiver //
  @Override public void onBluetoothTurnedOn() {
    MCBandLogger.d(TAG, "------ Bluetooth was turned on ------");
    btnEnableBluetooth.setEnabled(false);
    verifyReadyToPairBand();
  }

  // BaseFragment receives the bluetooth events through BroadcastReceiver //
  @Override public void onBluetoothTurningOn() {
    MCBandLogger.d(TAG, "------ Bluetooth is turning on ------");
  }

  // BaseFragment receives the band events through BroadcastReceiver //
  @Override public void onBandConnected() {
    btnConnectBand.setEnabled(false);

    ///// Go to next fragment to start playing with band ! ////
    ((FragmentEventListener) getActivity()).onFragmentEvent("SHOW_PLAYS_FRAGMENT");
  }

  // BaseFragment receives the band events through BroadcastReceiver //
  @Override public void onBandDisconnected() {
    Toast.makeText(getContext(), "Band is disconnected", Toast.LENGTH_SHORT).show();
    btnConnectBand.setEnabled(true);
  }
}