package com.mightycast.nexbandsample;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.mightycast.bandsdk.command.MCBandHapticCommand;
import com.mightycast.bandsdk.command.MCBandLEDRampCommand;
import com.mightycast.bandsdk.command.MCBandLedRampPersistentCommand;
import com.mightycast.bandsdk.command.MCBandLedRampTouchResponseCommand;
import com.mightycast.bandsdk.command.handler.MCBandCommandSequenceEventHandler;
import com.mightycast.bandsdk.command.handler.MCBandCommandSequencePlayerListener;
import com.mightycast.bandsdk.command.model.MCBandCommandSequence;
import com.mightycast.bandsdk.command.model.MCBandCommandSequenceEvent;
import com.mightycast.bandsdk.command.player.MCBandCommandSequencePlayer2;
import com.mightycast.bandsdk.command.player.MCBandPlayer;
import com.mightycast.bandsdk.command.player.MCBandPlayerEvent;
import com.mightycast.bandsdk.model.MCBand;
import com.mightycast.bandsdk.model.MCBandToken;
import com.mightycast.bandsdk.util.MCBandLogger;
import com.mightycast.lightpatterneditor.PatternEditorActivity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;
import static com.mightycast.bandsdk.command.model.MCBandCommandSequence.sequenceWithData;
import static com.mightycast.bandsdk.command.player.MCBandCommandSequencePlayer2.EVENT_END;
import static com.mightycast.bandsdk.notifications.MCBandTouchGestureNotificationHandler.EXTRA_NUMBER_OF_TAP;
import static com.mightycast.bandsdk.notifications.MCBandTouchGestureNotificationHandler.EXTRA_TOKEN_SLOT;
import static com.mightycast.bandsdk.notifications.MCBandTouchGestureNotificationHandler.LOCAL_BROADCAST_BASE_COVER_RECOGNIZED;
import static com.mightycast.bandsdk.notifications.MCBandTouchGestureNotificationHandler.LOCAL_BROADCAST_BASE_TAP_GESTURE_RECOGNIZED;
import static com.mightycast.bandsdk.notifications.MCBandTouchGestureNotificationHandler.LOCAL_BROADCAST_LONG_PRESS_GESTURE_RECOGNIZED;
import static com.mightycast.bandsdk.notifications.MCBandTouchGestureNotificationHandler.LOCAL_BROADCAST_SINGLE_TAP_RECOGNIZED;
import static com.mightycast.bandsdk.notifications.MCBandTouchGestureNotificationHandler.LOCAL_BROADCAST_TAPS_GESTURE_RECOGNIZED;

/**
 * Created by mojtabaa on 2017-03-29.
 */

public class BandPlaysFragment extends BaseFragment {
  private LinearLayout bandLayout;
  private TextView txtColorTitle;
  private View txtColor;
  private TextView txtSteps;
  private TextView txtLastEvents;
  private Button btnVibrate;
  private Button btnSetAllColors;
  private Button btnTurnOff;
  private Button btnForgetAndDisconnect;
  private Button btnDesign;
  private Button btnPlayCustomPattern;

  private static final int DURATION_MILLIS = 300;
  private int steps = 0;
  private int currentSlot = 0;
  private String[] latestPattern = {};

  public static BandPlaysFragment newInstance() {
    Bundle args = new Bundle();
    BandPlaysFragment fragment = new BandPlaysFragment();
    fragment.setArguments(args);
    return fragment;
  }

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {

    View view = inflater.inflate(R.layout.fragment_play_on_band, container, false);
    initializeViews(view);
    initializeBandColors();
    if (MCBand.getCurrentBand() != null) MCBand.getCurrentBand().setMotionEnabled(true);
    return view;
  }

  @Override public void onAttach(Context context) {
    super.onAttach(context);
    ///// Register receiver to get band touch events
    IntentFilter filter = new IntentFilter();
    filter.addAction(MCBandToken.ACTION_TOUCH_TRACKING_CHANGED);
    filter.addAction(LOCAL_BROADCAST_BASE_TAP_GESTURE_RECOGNIZED);
    filter.addAction(LOCAL_BROADCAST_TAPS_GESTURE_RECOGNIZED);
    filter.addAction(LOCAL_BROADCAST_SINGLE_TAP_RECOGNIZED);
    filter.addAction(LOCAL_BROADCAST_LONG_PRESS_GESTURE_RECOGNIZED);
    filter.addAction(LOCAL_BROADCAST_BASE_COVER_RECOGNIZED);
    filter.addAction(LOCAL_BROADCAST_BASE_COVER_RECOGNIZED);
    //context.registerReceiver(mBandEventReceiver, filter);
    LocalBroadcastManager.getInstance(context).registerReceiver(mBandEventReceiver, filter);
    ///// Register receiver to get band motion events
    filter = new IntentFilter();
    filter.addAction(MCBand.ACTION_MOTION_DATA_RECEIVED);
    LocalBroadcastManager.getInstance(context).registerReceiver(mMotionDataReceiver, filter);
  }

  @Override public void onDestroy() {
    LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBandEventReceiver);
    LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mMotionDataReceiver);
    super.onDestroy();
  }

  private void initializeViews(View view) {
    bandLayout = (LinearLayout) view.findViewById(R.id.band_layout);

    for (int i = 0; i < bandLayout.getChildCount(); i++) {
      View slot = bandLayout.getChildAt(i);
      slot.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
          int slot = getSlotByView(v);
          if (slot > 0) onSlotViewClicked(v, slot);
        }
      });
    }

    txtLastEvents = (TextView) view.findViewById(R.id.txt_last_event);
    txtSteps = (TextView) view.findViewById(R.id.txt_steps);
    txtColor = view.findViewById(R.id.txt_change_color);
    txtColorTitle = (TextView) view.findViewById(R.id.txt_color_title);
    btnVibrate = (Button) view.findViewById(R.id.btn_vibrate);
    btnSetAllColors = (Button) view.findViewById(R.id.btnSetAllColors);
    btnTurnOff = (Button) view.findViewById(R.id.btnTurnOff);
    btnDesign = (Button) view.findViewById(R.id.btnDesign);
    btnPlayCustomPattern = (Button) view.findViewById(R.id.btnPlayCustomPattern);

    btnVibrate.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        playVibrationOnBand();
      }
    });

    btnSetAllColors.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {

        turnOnLightsWithColors(MCBand.getCurrentBand(), getAllColors());
      }
    });

    btnTurnOff.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        turnOffLights(MCBand.getCurrentBand());
        // playSample(MCBand.getCurrentBase());
      }
    });

    btnDesign.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        showLightPatternEditor();
      }
    });

    btnPlayCustomPattern.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        playCustomPattern();
      }
    });

    txtColor.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        int color = ((ColorDrawable) v.getBackground()).getColor();
        showColorDialog(color);
      }
    });
  }

  private void initializeBandColors() {
    MCBandLogger.d(TAG, "Band Connected. Register Slot Colors on Band...");

    /// To have a better user experience, you can register color on each band slots so whenever
    /// you touch that slot (or press the power button on band) it lights up with that color.
    for (int slot = 1; slot <= 5; slot++)
      registerSlotColorOnBand(slot, getViewColorBySlot(slot));
  }

  private int getSlotByView(View v) {
    for (int i = 0; i < bandLayout.getChildCount(); i++) {
      if (v.equals(bandLayout.getChildAt(i))) return i + 1;
    }
    return 0;
  }

  private int getViewColorBySlot(int slot) {
    View slotView = bandLayout.getChildAt(slot - 1);
    return ((ColorDrawable) slotView.getBackground()).getColor();
  }

  private void onSlotViewClicked(View view, int slot) {
    int color = ((ColorDrawable) view.getBackground()).getColor();
    showColorOnBand(slot, color);
    txtColorTitle.setText("Slot " + slot + " Color:");
    txtColor.setBackgroundColor(color);
    currentSlot = slot;
  }

  private int[] getAllColors() {
    int nbrMods = bandLayout.getChildCount();
    int[] colors = new int[nbrMods];

    for (int i = 0; i < nbrMods; i++) {
      View slot = bandLayout.getChildAt(i);
      colors[i] = ((ColorDrawable) slot.getBackground()).getColor();
    }

    return colors;
  }

  /**
   * Creates a sequence of events to play on band. This sequence is lighting up one slot.
   * Every light up event has these properties:
   * ---- Slot (1 to 5), duration of play, color of slot, delay (start time) in millis
   * Note: Generally times are not recommended to be less than 100 Millis and be 100x (200,300,etc).
   */
  private void showColorOnBand(int slot, int color) {
    List<MCBandPlayerEvent> events = new ArrayList<>();
    List<MCBandCommandSequence> sequences = new ArrayList<>();

    events.add(new MCBandPlayerEvent().onSlot(slot)
        .setDuration(DURATION_MILLIS)
        .setColor(color)
        .setDelay(0));

    //// You can add multiple events using this builder ///

    sequences.add(MCBandPlayer.using(MCBand.getCurrentBand())
        .addEvents(events)
        .create()
        .getCommandSequence());

    playSequenceOnBand(sequences);
  }

  private boolean showColorsOnBand(MCBand base, final int[] colors) {

    if (!MCBand.isConnected() || colors == null) {
      return false;
    }

    if (colors.length != 5) {
      return false;
    }

    List<MCBandCommandSequenceEvent> commandSequenceEvents = new ArrayList<>(colors.length);
    for (int i = 0; i < colors.length; i++) {
      //Need to fix (i +1) for commandForTokenLightForever
      MCBandLedRampPersistentCommand onCommand =
          MCBandLedRampPersistentCommand.commandForTokenLightForever(i + 1, colors[i]);
      MCBandCommandSequenceEvent cmdSE = new MCBandCommandSequenceEvent(onCommand, 0);
      commandSequenceEvents.add(cmdSE);
    }

    MCBandCommandSequence sequence = new MCBandCommandSequence(commandSequenceEvents);
    playSequenceOnBand(Arrays.asList(sequence));

    new Handler().postDelayed(new Runnable() {
      @Override public void run() {
        turnOffLights(MCBand.getCurrentBand(), colors);
      }
    }, 60000);

    return true;
  }

  /**
   * To send Vibration to band you must use (MIGHapticCommand) like this:
   * - Create an instance of MIGHapticCommand. It takes 1 parameter (preset) which is the type of
   * vibration
   * - Create an event based on this HapticCommand
   * - Create a MCBandCommandSequence and add the event you just created to list of events that it
   * has
   */
  private void playVibrationOnBand() {
    // Create an instance of MIGHapticCommand. It takes 1 parameter (preset) which is the type of vibration
    // Preset could have different values. and based on these values can have longer duration and/or more/less strength
    MCBandHapticCommand cmd =
        new MCBandHapticCommand(MCBandHapticCommand.PresetStrongClick100Percent);
    // Create an event based on this HapticCommand
    MCBandCommandSequenceEvent cmdSeqEvent = new MCBandCommandSequenceEvent(cmd, 0);
    // Create a MCBandCommandSequence and add the event you just created to list of events that it has
    List<MCBandCommandSequence> sequences = new ArrayList<>();
    sequences.add(new MCBandCommandSequence());
    sequences.get(0).events.add(cmdSeqEvent);
    // Play the sequence on band using MCBandCommandSequencePlayer2
    playSequenceOnBand(sequences);
  }

  /**
   * To play a list of sequences on band you can use MCBandCommandSequencePlayer2
   * The player manages the mechanism to send sequences one after another, so you don't need to be
   * worried about when and how send next sequence.
   */
  private void playSequenceOnBand(List<MCBandCommandSequence> sequences) {
    MCBandCommandSequencePlayer2 player = new MCBandCommandSequencePlayer2();
    player.playSequences(sequences, MCBand.getCurrentBand(),
        new MCBandCommandSequencePlayerListener() {
          @Override public void handleEvent(MCBand base, int playerEvent) {
            if (playerEvent == EVENT_END) {
              MCBandLogger.d(TAG, "Playing Ended");
            }
          }
        });
  }

  /// Shows dialog to pick color ///
  private void showColorDialog(int currentColor) {
    if (currentSlot > 0) {
      ColorPickerDialog colorPickerDialog =
          new ColorPickerDialog(getContext(), new ColorPickerDialog.OnColorChangedListener() {
            @Override public void colorChanged(String key, int color) {
              /// You can change the slot color on band to any color value
              changeSlotColor(color);
            }
          }, "slot", currentColor, currentColor);

      colorPickerDialog.show();
    }
  }

  /// Changes the color of slot
  private void changeSlotColor(int color) {
    //// Update UI based on given color
    View slotView = bandLayout.getChildAt(currentSlot - 1);
    slotView.setBackgroundColor(color);
    txtColor.setBackgroundColor(color);

    //// Register the Color on band so whenever you touch that slot
    registerSlotColorOnBand(currentSlot, color);
  }

  /// Show latest log events /////
  private void logBandEvent(String event) {
    MCBandLogger.d(TAG, event);
    txtLastEvents.setText(event);
    txtLastEvents.invalidate();
  }

  /**
   * To have a better user experience, you can register color on each band slots so whenever
   * you touch that slot (or press the power button on band) it lights up with that color.
   */
  private void registerSlotColorOnBand(int slot, int color) {
    List<MCBandCommandSequenceEvent> sequenceEvents = new ArrayList<>();
    // Touch response Command is what you need when you want to register a color on band
    MCBandLedRampTouchResponseCommand command =
        MCBandLedRampTouchResponseCommand.ledRampCommandWithSlot(slot - 1, 200, color);
    MCBandCommandSequenceEvent cmdSE = new MCBandCommandSequenceEvent(command, 0);
    sequenceEvents.add(cmdSE);

    List<MCBandCommandSequence> sequences = new ArrayList<>();
    sequences.add(new MCBandCommandSequence(sequenceEvents));

    // You can execute your command on band using executeCommandSequence
    // MCBand.getCurrentBase() gives you access to connected band
    if (MCBand.getCurrentBand() != null) {
      MCBand.getCurrentBand()
          .executeCommandSequence(new MCBandCommandSequence(sequenceEvents), null);
    }
  }

  /// Just shows the total steps that has been received
  private void onStepDataReceived(int count) {
    steps += count;
    logBandEvent("Total Steps:" + steps);
    txtSteps.setText("Total Steps:" + steps);
  }

  /// This event receives through broadcast receiver that is defined in BaseFragment
  @Override public void onBluetoothTurnedOff() {
    ((FragmentEventListener) getActivity()).onFragmentEvent("SHOW_CONNECT_FRAGMENT");
  }

  /// This event receives through broadcast receiver that is defined in BaseFragment
  @Override public void onBluetoothTurningOff() {
  }

  /// This event receives through broadcast receiver that is defined in BaseFragment
  @Override public void onBluetoothTurnedOn() {
  }

  /// This event receives through broadcast receiver that is defined in BaseFragment
  @Override public void onBluetoothTurningOn() {
  }

  /// This event receives through broadcast receiver that is defined in BaseFragment
  @Override public void onBandConnected() {

  }

  /// This event receives through broadcast receiver that is defined in BaseFragment
  @Override public void onBandDisconnected() {

    ((FragmentEventListener) getActivity()).onFragmentEvent("SHOW_CONNECT_FRAGMENT");
  }

  /**
   * To handle the touch events receives from band
   * Band Cover happens when you cover all slots at the same time
   */
  private final BroadcastReceiver mBandEventReceiver = new BroadcastReceiver() {
    @Override public void onReceive(Context context, Intent intent) {
      String action = intent.getAction();
      MCBandLogger.d(TAG, "mBandEventReceiver triggered. action:" + action);

      //////// GESTURE    ////////////
      if (LOCAL_BROADCAST_BASE_TAP_GESTURE_RECOGNIZED.equalsIgnoreCase(action)) {
        MCBandLogger.d(TAG, "BAND GESTURE_RECOGNIZED");
        return;
      }

      //////// BAND COVER ////////////
      if (LOCAL_BROADCAST_BASE_COVER_RECOGNIZED.equalsIgnoreCase(action)) {
        MCBandLogger.d(TAG, "BAND COVER");
        logBandEvent("BAND COVER");
        return;
      }

      //////// BAND TRACKING ///////
      if (MCBandToken.ACTION_TOUCH_TRACKING_CHANGED.equalsIgnoreCase(action)) {
        int slotNumber = intent.getIntExtra(EXTRA_TOKEN_SLOT, 0);
      }

      //////// BAND SINGLE TAP ///////
      if (LOCAL_BROADCAST_SINGLE_TAP_RECOGNIZED.equalsIgnoreCase(action)) {
        int slotNumber = intent.getIntExtra(EXTRA_TOKEN_SLOT, 0);
        logBandEvent("SINGLE TAP on " + slotNumber);
        return;
      }

      //////// BAND MULTI TAP ///////
      if (LOCAL_BROADCAST_TAPS_GESTURE_RECOGNIZED.equalsIgnoreCase(action)) {
        int nbrTaps = intent.getIntExtra(EXTRA_NUMBER_OF_TAP, 1);
        int slotNumber = intent.getIntExtra(EXTRA_TOKEN_SLOT, 0);
        //if (nbrTaps == 2) {
        //  logBandEvent("DOUBLE TAP on " + slotNumber);
        //}
        //if (nbrTaps == 3) {
        //  logBandEvent("TRIPLE TAP on " + slotNumber);
        //}
        logBandEvent("TAP(" + nbrTaps + " times) on " + slotNumber);
        return;
      }

      //////// BAND LONG PRESS /////
      if (LOCAL_BROADCAST_LONG_PRESS_GESTURE_RECOGNIZED.equalsIgnoreCase(action)) {
        int slotNumber = intent.getIntExtra(EXTRA_TOKEN_SLOT, 0);
        logBandEvent("LONG PRESS on " + slotNumber);
      }
    }
  };

  /**
   * Receives the band motion events.
   */
  private BroadcastReceiver mMotionDataReceiver = new BroadcastReceiver() {
    @Override public void onReceive(Context context, Intent intent) {
      String action = intent.getAction();
      switch (action) {
        case MCBand.ACTION_MOTION_DATA_RECEIVED:
          int count = intent.getIntExtra(MCBand.EXTRA_STEP_COUNT, 0);
          onStepDataReceived(count);
      }
    }
  };

  /**
   * Lights up all the mods using the given colors
   */
  public boolean turnOnLightsWithColors(MCBand base, int[] colors) {
    if (!MCBand.isConnected() || colors == null) {
      return false;
    }

    if (colors.length != 5) {
      return false;
    }

    List<MCBandCommandSequenceEvent> sequenceEvents = new ArrayList<>(colors.length);
    for (int i = 0; i < colors.length; i++) {
      MCBandCommandSequenceEvent sequenceEvent = new MCBandCommandSequenceEvent(
          MCBandLedRampPersistentCommand.commandForTokenLightForever(i + 1, colors[i]), 0);
      sequenceEvents.add(sequenceEvent);
    }

    base.executeCommandSequence(new MCBandCommandSequence(sequenceEvents),
        new MCBandCommandSequenceEventHandler() {
          @Override public void onEvent(MCBandCommandSequence sequence, int state) {
          }
        });

    return true;
  }

  /**
   * TUrns off all lights on the band
   */
  public boolean turnOffLights(MCBand base) {
    return turnOffLights(base, getAllColors());
  }

  public void playSample(MCBand base) {
    if (base == null) return;
    int commandType = MCBandLEDRampCommand.MIGLedRampPersistentCommandKeepAliveNone;
    List<MCBandCommandSequenceEvent> commandSequenceEvents = new ArrayList<>();
    MCBandLEDRampCommand command;
    MCBandCommandSequenceEvent cmdSE;
    int startTime = 0;
    int lightUpDuration = 200;

    /// Right to left
    for (int i = 0; i < 5; i++) {
      startTime += lightUpDuration;
      command = new MCBandLEDRampCommand(i, lightUpDuration, Color.BLACK, Color.RED, commandType);
      cmdSE = new MCBandCommandSequenceEvent(command, startTime);
      commandSequenceEvents.add(cmdSE);
    }

    /// Left to right
    for (int i = 4; i >= 0; i--) {
      startTime += lightUpDuration;
      command = new MCBandLEDRampCommand(i, lightUpDuration, Color.BLACK, Color.RED, commandType);
      cmdSE = new MCBandCommandSequenceEvent(command, startTime);
      commandSequenceEvents.add(cmdSE);
    }

    base.executeCommandSequence(new MCBandCommandSequence(commandSequenceEvents), null);
  }

  public boolean turnOffLights(MCBand base, int[] colors) {
    if (!MCBand.isConnected()) {
      return false;
    }

    int size = colors.length;
    List<MCBandCommandSequenceEvent> sequenceEvents = new ArrayList<>(size);
    for (int i = 0; i < size; i++) {
      //Setting the color to black turns them off
      int color = colors[i];

      //Passing the current color to lights off will have a nice fade out effect.
      MCBandCommandSequenceEvent sequenceEvent = new MCBandCommandSequenceEvent(
          MCBandLedRampPersistentCommand.commandForTokenLightOff(i, color), 0);
      sequenceEvents.add(sequenceEvent);
    }

    base.executeCommandSequence(new MCBandCommandSequence(sequenceEvents), null);

    return true;
  }

  private void showLightPatternEditor() {
    Intent intent = new Intent(getContext(), PatternEditorActivity.class);
    intent.putExtra(PatternEditorActivity.SEQUENCES_BASE64, latestPattern);
    startActivityForResult(intent, 121);
  }

  @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == RESULT_OK && data.hasExtra(PatternEditorActivity.SEQUENCES_BASE64)) {
      latestPattern = data.getStringArrayExtra(PatternEditorActivity.SEQUENCES_BASE64);
    }
  }

  private void playCustomPattern() {
    if (latestPattern != null && latestPattern.length > 0) {
      MCBandCommandSequence sequence =
          sequenceWithData(Base64.decode(latestPattern[0], Base64.DEFAULT));
      MCBand.getCurrentBand().executeCommandSequence(sequence, null);
    }
  }
}
