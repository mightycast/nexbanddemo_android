package com.mightycast.nexbandsample;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import org.w3c.dom.Text;

/**
 * Created by mojtabaa on 2017-03-29.
 */

public class ColorPickerDialog extends Dialog {

  public interface OnColorChangedListener {
    void colorChanged(String key, int color);
  }

  private SeekBar sbRed;
  private SeekBar sbGreen;
  private SeekBar sbBlue;
  private Button btnSelect;
  private TextView txtColor;
  private OnColorChangedListener mListener;
  private int mInitialColor, mDefaultColor;
  private String mKey;

  public ColorPickerDialog(Context context, OnColorChangedListener listener, String key,
      int initialColor, int defaultColor) {
    super(context);

    mListener = listener;
    mKey = key;
    mInitialColor = initialColor;
    mDefaultColor = defaultColor;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    OnColorChangedListener l = new OnColorChangedListener() {
      public void colorChanged(String key, int color) {
        mListener.colorChanged(mKey, color);
        dismiss();
      }
    };
    setContentView(R.layout.dialog_color);

    sbRed = (SeekBar) findViewById(R.id.sbRed);
    sbGreen = (SeekBar) findViewById(R.id.sbGreen);
    sbBlue = (SeekBar) findViewById(R.id.sbBlue);
    txtColor = (TextView) findViewById(R.id.txt_color_preview);
    btnSelect = (Button) findViewById(R.id.btnSelect);

    btnSelect.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        if (mListener != null) {
          mListener.colorChanged(mKey,
              Color.argb(255, sbRed.getProgress(), sbGreen.getProgress(), sbBlue.getProgress()));
        }
        dismiss();
      }
    });

    txtColor.setBackgroundColor(mInitialColor);
    sbRed.setProgress(Color.red(mInitialColor));
    sbGreen.setProgress(Color.green(mInitialColor));
    sbBlue.setProgress(Color.blue(mInitialColor));
    SeekBar.OnSeekBarChangeListener seekListener = new SeekBar.OnSeekBarChangeListener() {
      @Override public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        setPreviewColor();
      }

      @Override public void onStartTrackingTouch(SeekBar seekBar) {

      }

      @Override public void onStopTrackingTouch(SeekBar seekBar) {

      }
    };
    sbRed.setOnSeekBarChangeListener(seekListener);
    sbGreen.setOnSeekBarChangeListener(seekListener);
    sbBlue.setOnSeekBarChangeListener(seekListener);

    setTitle("Pick Color");
  }

  private void setPreviewColor() {
    int red = sbRed.getProgress();
    int green = sbGreen.getProgress();
    int blue = sbBlue.getProgress();
    txtColor.setBackgroundColor(Color.argb(255, red, green, blue));
  }
}